var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

getJSON('https://newsapi.org/v2/top-headlines?sources=football-italia&apiKey=7d73dd44453e4453847df953b01dbbf5',
    function (err, data) {
        
        console.log(data['articles'][0]);

        for ( var i = 0 ; i < data['articles'].length ; i ++)
        {
            $("#news-row-1").append(
                '<div class="card col-xs-4" ><img class="card-img-top" style="height:180px" src=' + data['articles'][i]['urlToImage'] + ' alt="Card image cap"><div ><h5 class="card-title">' + data['articles'][i]['title'] + '</h5><a target="_blank" href='+data['articles'][i]['url']+' class="btn btn-primary">View More</a></div></div>')
        
        }

    });

getJSON('https://newsapi.org/v2/top-headlines?sources=the-sport-bible&apiKey=7d73dd44453e4453847df953b01dbbf5',
    function (err, data) {

        console.log(data['articles'][0]);

        for (var i = 0; i < data['articles'].length; i++) {
            $("#news-row-2").append(
                '<div class="card col-xs-4" ><img class="card-img-top" style="height:180px" src=' + data['articles'][i]['urlToImage'] + ' alt="Card image cap"><div ><h5 class="card-title">' + data['articles'][i]['title'] + '</h5><a target="_blank" href=' + data['articles'][i]['url'] + ' class="btn btn-primary">View More</a></div></div>')

        }

    });


$(document).ready(function () {
    for (var i = 0; i < 5; i++)
        $("#NewsDiv").append("<div class='row'>Test</div>");
});