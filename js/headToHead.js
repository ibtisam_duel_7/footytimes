var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    
    xhr.open('GET', url, true);
    xhr.setRequestHeader('X-Auth-Token', 'f78bca4fad71497586a22633b6ebf7a0');

    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_head_leagues;

var all_head_teams;

var all_fixtures;

var getHeadLeagues = function () {


    getJSON('http://api.football-data.org/v1/competitions/?season=2017',
        function (err, data) {

            console.log(data);

            all_head_leagues = data;

            var x = document.getElementById("selectHeadTeamLeague");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            for (var i = 0; i < data.length; i++) {

                var option = document.createElement("option");
                option.text = data[i]['caption'];
                x.add(option, x[i]);
            }

            getHeadTeams(0);


        });

}

var getHeadTeams = function (index) {
    var id = all_head_leagues[index]['id'];

    getJSON('http://api.football-data.org/v1/competitions/' + id + '/teams',
        function (err, data) {

            console.log(data);


            var x = document.getElementById("selectHeadTeam");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            data = data['teams']

            all_head_teams = data;


            for (var i = 0; i < data.length; i++) {

                var option = document.createElement("option");
                option.text = data[i]['name'];
                x.add(option, x[i]);
            }

            getHeadFixtures();

        });

}

var refreshHeadTeams = function () {

    var index = document.getElementById("selectHeadTeamLeague").selectedIndex;

    getHeadTeams(index);

}


var getHeadFixtures = function () {

    var index = document.getElementById("selectHeadTeam").selectedIndex;

    var url = all_head_teams[index]['_links']['fixtures']['href'];
    console.log(url);

    getJSON(url,
        function (err, data) {

            console.log("match fixtures data");
            console.log(data);

            var dataTable = $('#headToHeadTable').DataTable();

            data = data['fixtures'];

            all_fixtures = data;

            dataTable.clear();

            for (var i = 0; i < data.length; i++) {

                dataTable.row.add([
                    data[i]["awayTeamName"],
                    data[i]["homeTeamName"],
                    data[i]["date"],
                    '<button type = "button" class= "btn btn-primary show-head" style = "margin-right:16px;" > <span>Show head to head</span> </button>',

                ]).draw();
            }


            $('.show-head').each(function () {
                $(this).on('click', function (evt) {
                    $this = $(this);
                    var dtRow = $this.parents('tr');
                    
                    console.log("clicked row is " + dtRow[0].rowIndex);

                    

                    getHeadToHead(all_fixtures[dtRow[0].rowIndex]['_links']['self']['href']);
                    
                });
            });

           
              
               
            
            //.ajax.reload();


        });

}

var getHeadToHead = function (url) {

    getJSON(url,
        function (err, data) {

            console.log("match head to head data");
            console.log(data);

            document.getElementById("fixture-count").innerHTML = 'Total Count: ' + data['head2head']['count'];
            document.getElementById("away-team-win").innerHTML = 'Away Team wins: ' + data['head2head']['awayTeamWins'];
            document.getElementById("home-team-win").innerHTML = 'Home Team wins: ' + data['head2head']['homeTeamWins'];

            document.getElementById("draws").innerHTML = 'Draws: ' + data['head2head']['draws'];

            var modeldataTable = $('#modalheadToHeadTable').DataTable();

            data = data['head2head']['fixtures'];

            modeldataTable.clear();

            for (var i = 0; i < data.length; i++) {

                modeldataTable.row.add([
                    data[i]["awayTeamName"],
                    data[i]["homeTeamName"],
                    data[i]["date"],
                    data[i]["result"]["goalsAwayTeam"],
                    data[i]["result"]["goalsHomeTeam"],
                    data[i]["status"]

                ]).draw();
            }


                    $('#myModal').modal('show');

           
           
            //.ajax.reload();


        });
}




getHeadLeagues();