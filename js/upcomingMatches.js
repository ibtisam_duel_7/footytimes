var getupcomingJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
     xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_tournaments;

var getTournaments = function()
{
    getupcomingJSON('https://push.sportskeeda.com/get-football-matches/fifa-world-cup-2018',
        function (err, data) {

            console.log("upcoming match data");

            
            var x = document.getElementById("upcomingMatchLeague");

            console.log(data);

            all_tournaments = data["tournaments"];

            for(var i = 0 ; i < all_tournaments.length ; i ++)
            {

            var option = document.createElement("option");
            option.text = all_tournaments[i]["tournament_name"];
            
            x.add(option, x[i]);
        }

        get_match_schedule();

        });
}


var get_match_schedule = function()
{
    var tournament = all_tournaments[document.getElementById("upcomingMatchLeague").selectedIndex]["tournament_slug"]

    getupcomingJSON('https://push.sportskeeda.com/get-football-matches/'+tournament,
        function (err, data) {

            console.log("tournament data");

            console.log(data);

            var dataTable = $('#upcomingMatchesTable').DataTable();

            data = data['tournament_matches'][0]['scores'];


            dataTable.clear();

            for (var i = 0; i < data.length; i++) {

                dataTable.row.add([
                    data[i]["date"],
                    data[i]["time"],
                    '<div> <img style="width: 40px ; height: 40px" src="' + data[i]["t1_logo"] + '" > <span>' + data[i]["t1"]+'</span>  </div>',
                    'Vs',
                    '<div> <img style="width: 40px ; height: 40px" src="' + data[i]["t2_logo"] + '" > <span>' + data[i]["t2"] + '</span>  </div>',



                ]).draw();
            }

        });

}

getTournaments();