var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.setRequestHeader('X-Auth-Token', 'f78bca4fad71497586a22633b6ebf7a0');

    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_FanZone_leagues;

var all_FanZone_teams;

var all_fixtures;

var getFanZoneLeagues = function () {


    getJSON('http://api.football-data.org/v1/competitions/?season=2017',
        function (err, data) {

            console.log(data);

            all_FanZone_leagues = data;

            var x = document.getElementById("selectFanZoneTeamLeague");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            for (var i = 0; i < data.length; i++) {

                var option = document.createElement("option");
                option.text = data[i]['caption'];
                x.add(option, x[i]);
            }

            getFanZoneTeams(0);


        });

}

var getFanZoneTeams = function (index) {
    var id = all_FanZone_leagues[index]['id'];

    getJSON('http://api.football-data.org/v1/competitions/' + id + '/teams',
        function (err, data) {

            console.log(data);


            var x = document.getElementById("selectFanZoneTeam");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            data = data['teams']

            all_FanZone_teams = data;


            for (var i = 0; i < data.length; i++) {

                var option = document.createElement("option");
                option.text = data[i]['name'];
                x.add(option, x[i]);
            }

            getFanZonePlayers();

        });

}

var refreshFanZoneTeams = function () {

    var index = document.getElementById("selectFanZoneTeamLeague").selectedIndex;

    getFanZoneTeams(index);

}


var getFanZonePlayers = function () {

    var index = document.getElementById("selectFanZoneTeam").selectedIndex;

    var url = all_FanZone_teams[index]['_links']['players']['href'];
    console.log(url);

    getJSON(url,
        function (err, data) {

            console.log("players data");
            console.log(data);

            var dataTable = $('#FanZonePlayerTable').DataTable();

            data = data['players'];

            all_fixtures = data;

            dataTable.clear();

            for (var i = 0; i < data.length; i++) {

            

                dataTable.row.add([
                    data[i]["name"],
                    data[i]["position"],
                    data[i]["jerseyNumber"],
                    data[i]["dateOfBirth"],
                    data[i]["nationality"],
                    data[i]["contractUntil"]
                    
                    
                ]).draw();
            }


           



        });

}




getFanZoneLeagues();