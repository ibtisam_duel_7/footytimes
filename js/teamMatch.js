var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.setRequestHeader('X-Auth-Token', 'f78bca4fad71497586a22633b6ebf7a0');

    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_leagues ;

var all_teams ;

var all_games_result;

var getLeagues = function () {

    
    getJSON('http://api.football-data.org/v1/competitions/?season=2018',
        function (err, data) {

            console.log(data);

            all_leagues = data;

            getJSON('http://api.football-data.org/v1/competitions/?season=2017',
                function (err2, data2) {

                    Array.prototype.push.apply(data, data2);
                    all_leagues = data;

            var x = document.getElementById("selectTeamLeague");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            for (var i = 0; i < all_leagues.length; i++) {

                var option = document.createElement("option");
                option.text = all_leagues[i]['caption'];
                x.add(option, x[i]);
            }

            getTeams(0);
        });
            

        });

}

var getTeams = function (index )
{
    var id = all_leagues[index]['id'];

    getJSON('http://api.football-data.org/v1/competitions/'+id+'/teams',
        function (err, data) {

            console.log(data);

             
            var x = document.getElementById("selectTeam");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            data = data['teams']

            all_teams = data;


            for (var i = 0; i < data.length; i++) {

                var option = document.createElement("option");
                option.text = data[i]['name'];
                x.add(option, x[i]);
            }

            getTeamMatch();

        });

}

var refreshTeams = function()  {

    var index = document.getElementById("selectTeamLeague").selectedIndex;

    getTeams(index);

}


var getTeamMatch = function () {

    var index = document.getElementById("selectTeam").selectedIndex;
    
    var url = all_teams[index]['_links']['fixtures']['href'];
    console.log(url);

    getJSON(url,
        function (err, data) {

            document.getElementById("filterTeamResults").selectedIndex = 0 ;

            console.log("match fixtures data");
            console.log(data);

            var dataTable = $('#fixtureTable').DataTable();

            data = data['fixtures'];

            dataTable.clear();

            all_games_result = data;

            for (var i = 0; i < data.length; i++) {

                dataTable.row.add([
                    data[i]["awayTeamName"],
                    data[i]["homeTeamName"],
                    data[i]["date"],
                    data[i]["matchday"],
                    data[i]["result"]["goalsHomeTeam"],
                    data[i]["result"]["goalsAwayTeam"]
                    
                ]).draw();
            }
           //.ajax.reload();

            $('#fixtureTable').DataTable().draw();
            
        });

    }


    getLeagues();



    var show_all_game_results = function () {
        
        var dataTable = $('#fixtureTable').DataTable();

       
        dataTable.clear();

        for (var i = 0; i < all_games_result.length; i++) {

            dataTable.row.add([
                all_games_result[i]["awayTeamName"],
                all_games_result[i]["homeTeamName"],
                all_games_result[i]["date"],
                all_games_result[i]["matchday"],
                all_games_result[i]["result"]["goalsHomeTeam"],
                all_games_result[i]["result"]["goalsAwayTeam"]

            ]).draw();
        }
        //.ajax.reload();

        $('#fixtureTable').DataTable().draw();

    }

var show_last_game_results = function () {

    var dataTable = $('#fixtureTable').DataTable();


    dataTable.clear();

    all_games_result.sort((a, b) => parseFloat(a.matchday) - parseFloat(b.matchday));

    var last_index = all_games_result.length -1 ;

    for (var i = 0; i < 1; i++) {

        dataTable.row.add([
            all_games_result[last_index]["awayTeamName"],
            all_games_result[last_index]["homeTeamName"],
            all_games_result[last_index]["date"],
            all_games_result[last_index]["matchday"],
            all_games_result[last_index]["result"]["goalsHomeTeam"],
            all_games_result[last_index]["result"]["goalsAwayTeam"]

        ]).draw();
    }
    //.ajax.reload();

    $('#fixtureTable').DataTable().draw();

}

var show_last_five_game_results = function () {

    var dataTable = $('#fixtureTable').DataTable();


    dataTable.clear();

    all_games_result.sort((a, b) => parseFloat(a.matchday) - parseFloat(b.matchday));

    var last_index = all_games_result.length - 1;

    for (var i = last_index; i > last_index - 5; i --) {

        dataTable.row.add([
            all_games_result[i]["awayTeamName"],
            all_games_result[i]["homeTeamName"],
            all_games_result[i]["date"],
            all_games_result[i]["matchday"],
            all_games_result[i]["result"]["goalsHomeTeam"],
            all_games_result[i]["result"]["goalsAwayTeam"]

        ]).draw();
    }
    //.ajax.reload();

    $('#fixtureTable').DataTable().draw();

}


var filter_game_match_result = function()
{

    var index = document.getElementById("filterTeamResults").selectedIndex;

    if(index == 0)
    {
        show_all_game_results();
    }

    else if(index == 1)
    {
        show_last_game_results();
    }

    else{
        show_last_five_game_results();
    }

}