var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.setRequestHeader('X-Auth-Token', 'f78bca4fad71497586a22633b6ebf7a0');


     xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_tournaments;

var getTournaments = function()
{
    getJSON('http://api.football-data.org/v1/competitions/?season=2018',
        function (err, data) {

            console.log(data);

        

            getJSON('http://api.football-data.org/v1/competitions/?season=2017',
                function (err2, data2) {

                    Array.prototype.push.apply(data, data2);
                    all_tournaments = data;

                    var x = document.getElementById("LeagueTeams_");

                    var j;
                    for (j = x.options.length - 1; j >= 0; j--) {
                        x.remove(j);
                    }

                    for (var i = 0; i < all_tournaments.length; i++) {

                        var option = document.createElement("option");
                        option.text = all_tournaments[i]['caption'];
                        x.add(option, x[i]);
                    }

                   get_teams_();
                });


        });

}


var get_teams_ = function()
{
    var id = all_tournaments[document.getElementById("LeagueTeams_").selectedIndex]["id"];

    getJSON('http://api.football-data.org/v1/competitions/' + id +'/teams',
        function (err, data) {

            console.log("teams data");

            console.log(data);

            var dataTable = $('#leagueTeamTable_').DataTable();

            data = data['teams'];


            dataTable.clear();

            for (var i = 0; i < data.length; i++) {

                dataTable.row.add([
                    data[i]["name"],
                    data[i]["code"],
                    '<img style="width: 40px ; height: 40px" src="' + data[i]["crestUrl"] + '" alt="team image" >',
                    data[i]["shortName"]


                ]).draw();
            }

        });

}

getTournaments();