var getJSON = function (url, callback) {
    var xhr = new XMLHttpRequest();
    
    xhr.open('GET', url, true);
    xhr.setRequestHeader('X-Auth-Token', 'f78bca4fad71497586a22633b6ebf7a0');

    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
};

var all_leagues ;

var getCompetition = function () {

    var year = document.getElementById("selectYear").value;
    
    getJSON('http://api.football-data.org/v1/competitions/?season='+year,
        function (err, data) {

            console.log(data);

            all_leagues = data;

            var x = document.getElementById("selectLeague");

            var j;
            for (j = x.options.length - 1; j >= 0; j--) {
                x.remove(j);
            }

            for( var i = 0 ; i < data.length ; i ++)
            {
                
                var option = document.createElement("option");
                option.text = data[i]['caption'];
                x.add(option, x[i]);
            }

            getLeagueTable(0)

        });
    
}

var getLeagueTable = function (index)
{
   var id = all_leagues[index]['id']

getJSON('http://api.football-data.org/v1/competitions/'+id+'/leagueTable',
    function (err, data) {

        console.log("league table data");
        console.log(data);

        data = data["standing"];

        var leaguedataTable = $('#leagueTable').DataTable();

        leaguedataTable.clear();

        for (var i = 0; i < data.length; i++) {
            
            leaguedataTable.row.add([
            data[i]["position"],
            data[i]["teamName"],
            data[i]['playedGames'],
            data[i]["wins"],
            data[i]["losses"],
            data[i]['draws'],
            data[i]['goals'],
            data[i]['goalsAgainst'],
            data[i]['points']

            ]).draw();

        }
        
        

    });
}

var refreshLeagueTable = function()
{
    var competitionId = document.getElementById("selectLeague").selectedIndex;
    getLeagueTable(competitionId);
}


getCompetition();